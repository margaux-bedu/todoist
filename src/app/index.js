import React from "react";
import "./index.scss";

import Header from "../components/Header/index";
import Main from "../pages/Main/index";

function App() {
  return (
    <div className="app-container">
      <Header />
      <Main />
    </div>
  );
}

export default App;
