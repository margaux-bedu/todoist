import React from "react";
import { connect } from "react-redux";

import "./index.scss";

import SideList from "../../components/SideList";
import Options from "../../components/Options";

class Main extends React.Component {
  render() {
    const { selectId } = this.props;

    return (
      <div className="main">
        <SideList />
        <div className={selectId !== null ? "show" : "hide"}>
          <Options />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  selectId: state.todo.selectId,
});

export default connect(mapStateToProps, null)(Main);
