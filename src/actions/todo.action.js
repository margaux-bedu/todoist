import * as todoConst from "../const/todo.const";

export const setSelect = (todo, id) => {
  return {
    type: todoConst.SET_SELECT,
    payload: { todo, id },
  };
};

export function selectLastTodo() {
  return (dispatch, getState) => {
    const { todos } = getState();
    const lastTodo = todos[todos.length - 1];

    return dispatch(setSelect(lastTodo, lastTodo.id));
  };
}

export const setTodoDescription = (description) => {
  return {
    type: todoConst.SET_TODO_DESCRIPTION,
    payload: description,
  };
};

export const setTodoUser = (name) => {
  return {
    type: todoConst.SET_TODO_USER,
    payload: name,
  };
};

export const setTodoDate = (date) => {
  return {
    type: todoConst.SET_TODO_DATE,
    payload: date,
  };
};

export const deleteTodoDate = () => {
  return {
    type: todoConst.DELETE_TODO_DATE,
  };
};
export const deleteTodoUser = () => {
  return {
    type: todoConst.DELETE_TODO_USER,
  };
};
