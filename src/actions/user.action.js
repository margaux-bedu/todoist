import * as userConst from "../const/user.const";

export const toggleUser = () => {
  return {
    type: userConst.TOGGLE_USER,
  };
};
