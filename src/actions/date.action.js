import * as dateConst from "../const/date.const";

export const toggleDate = () => {
  return {
    type: dateConst.TOGGLE_DATE,
  };
};

export const setValue = (date) => {
  return {
    type: dateConst.SET_VALUE,
    payload: date,
  };
};
