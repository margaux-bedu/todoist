import * as todosConst from "../const/todos.const";

export const addTodo = (title) => ({
  type: todosConst.ADD_TODO,
  payload: { title, id: Date.now() },
});

export const toggleTodo = (id) => {
  return {
    type: todosConst.TOGGLE_TODO,
    payload: id,
  };
};

export const setUser = (name, id) => {
  return {
    type: todosConst.SET_USER,
    payload: { name, id },
  };
};

export const setTodosDate = (date, id) => {
  return {
    type: todosConst.SET_DATE,
    payload: { date, id },
  };
};

export const setDescription = (description, id) => {
  return {
    type: todosConst.SET_DESCRIPTION,
    payload: { description, id },
  };
};

export const deleteTodosDate = (id) => {
  return {
    type: todosConst.DELETE_DATE,
    payload: id,
  };
};

export const deleteTodosUser = (id) => {
  return {
    type: todosConst.DELETE_USER,
    payload: id,
  };
};
