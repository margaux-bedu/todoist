import React from "react";
import PropTypes from "prop-types";

import "./index.scss";

const TaskCounter = ({ count }) => {
  return (
    <div className="counter-wrapper">
      <p>Tâches terminées</p>
      <p className="counter">{count}</p>
    </div>
  );
};

export default TaskCounter;

TaskCounter.propTypes = {
  count: PropTypes.number.isRequired,
};
