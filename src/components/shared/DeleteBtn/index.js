import React, { Component } from "react";
import PropTypes from "prop-types";

import "./index.scss";

class DeleteBtn extends Component {
  render() {
    const { handleDelete } = this.props;

    return (
      <button className="delete-btn" onClick={handleDelete}>
        Supprimer
      </button>
    );
  }
}

export default DeleteBtn;

DeleteBtn.propTypes = {
  handleDelete: PropTypes.func.isRequired,
};
