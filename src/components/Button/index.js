import React from "react";
import PropTypes from "prop-types";

import "./index.scss";

const Button = ({ icon, onclick, title }) => {
  return (
    <button onClick={onclick} className="btn" type="button">
      <div className="icon">
        {icon ? <img src="/asset/clock.png" alt="icon" /> : "+"}
      </div>
      {title}
    </button>
  );
};

export default Button;

Button.propTypes = {
  icon: PropTypes.bool,
  title: PropTypes.string,
  onclick: PropTypes.func.isRequired,
};
