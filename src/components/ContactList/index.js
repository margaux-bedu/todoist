import React from "react";
import { connect } from "react-redux";

import { deleteTodoUser } from "../../actions/todo.action";
import { deleteTodosUser } from "../../actions/todos.action";

import DeleteBtn from "../shared/DeleteBtn";

import "./index.scss";

class ContactList extends React.Component {
  render() {
    const { users } = this.props;
    const { setUser, id, toggleUser, setTodoUser } = this.props;
    const { deleteTodoUser, deleteTodosUser } = this.props;

    const handleDelete = () => {
      deleteTodoUser();
      deleteTodosUser(id);
      toggleUser();
    };

    return (
      <div className="contact-container">
        {users.map((user, index) => {
          return (
            <div
              key={index}
              className="contact-wrapper"
              onClick={() => {
                setUser(user.userName, id);
                setTodoUser(user.userName);
                toggleUser();
              }}
            >
              <img src={user.userAvatar} alt="avatar" />
              <div className="contact-content">
                <p className="contact-name">{user.userName}</p>
                <p className="contact-mail">{user.userMail}</p>
              </div>
            </div>
          );
        })}
        <DeleteBtn handleDelete={handleDelete} />
      </div>
    );
  }
}

const mapStatetoProps = (state) => ({
  users: state.user.users,
});

const mapDispatchToProps = (dispatch) => ({
  deleteTodoUser: () => dispatch(deleteTodoUser()),
  deleteTodosUser: (id) => dispatch(deleteTodosUser(id)),
});

export default connect(mapStatetoProps, mapDispatchToProps)(ContactList);
