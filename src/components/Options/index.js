import React from "react";
import moment from "moment";
import { connect } from "react-redux";

import Button from "../Button";
import ContactList from "../ContactList";
import DatePicker from "../DatePicker";

import {
  setUser,
  setDescription,
  toggleTodo,
} from "../../actions/todos.action";
import { setTodoUser, setTodoDescription } from "../../actions/todo.action";
import { toggleUser } from "../../actions/user.action";
import { toggleDate } from "../../actions/date.action";

import "./index.scss";

class Options extends React.Component {
  render() {
    const { todo, userSelect, showDate, value } = this.props;

    const {
      setUser,
      setDescription,
      toggleTodo,
      toggleUser,
      setTodoUser,
      setTodoDescription,
      toggleDate,
    } = this.props;

    const handleSubmit = () => {
      setDescription(todo.description, todo.id);
    };

    const displayDate = (date) => {
      if (
        moment(new Date()).isSame(date, "day") &&
        moment(new Date()).isSame(date, "month")
      )
        return "Aujourd'hui";
      else return moment(date).format("DD/MM/YYYY");
    };

    return (
      <div className="opt-container">
        <div className="opt-title">
          <h3>{todo.title}</h3>
          <button
            onClick={() => {
              toggleTodo(todo.id);
            }}
            type="button"
          >
            <img src="/asset/gray-check.png" alt="icon" />
            Marqué comme terminée
          </button>
        </div>
        <div className="opt-content">
          <div className="btn-wrapper">
            <Button
              onclick={() => {
                toggleUser();
                if (showDate) {
                  toggleDate();
                }
              }}
              title={todo.user !== null ? todo.user : "attribuer à"}
            />
            <Button
              icon
              onclick={() => {
                toggleDate();
                if (userSelect) {
                  toggleUser();
                }
              }}
              title={todo.date === "" ? "échéance" : displayDate(todo.date)}
            />
          </div>
          {userSelect ? (
            <div className="user-modal">
              <ContactList
                setTodoUser={setTodoUser}
                setUser={setUser}
                toggleUser={toggleUser}
                id={todo.id}
              />
            </div>
          ) : null}
          {showDate ? (
            <div className="date-modal">
              <DatePicker value={value} id={todo.id} />
            </div>
          ) : null}
          <div className="desc-container">
            <div className="desc-title-wrapper">
              <img src="/asset/icon-create.png" alt="icon" />
              <p>Description</p>
            </div>
            <form onSubmit={(e) => e.preventDefault()}>
              <textarea
                autoComplete="off"
                className="text-area"
                value={todo.description}
                placeholder={"Ajouter une description"}
                name="description"
                onChange={(e) => setTodoDescription(e.target.value)}
              />
              <div className="desc-btn-wrapper">
                <input
                  onClick={() => handleSubmit()}
                  type="submit"
                  value="commenter"
                />
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  userSelect: state.user.userSelect,
  todo: state.todo.todo,
  showDate: state.date.showDate,
  value: state.date.value,
});

const mapDispatchToProps = (dispatch) => ({
  setUser: (name, id) => dispatch(setUser(name, id)),
  setDescription: (description, id) =>
    dispatch(setDescription(description, id)),
  toggleTodo: (id) => dispatch(toggleTodo(id)),
  toggleUser: () => dispatch(toggleUser()),
  setTodoUser: (name) => dispatch(setTodoUser(name)),
  setTodoDescription: (description) =>
    dispatch(setTodoDescription(description)),
  toggleDate: () => dispatch(toggleDate()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Options);
