import React from "react";
import { connect } from "react-redux";
import moment from "moment";
import "moment/locale/fr";

import { toggleDate, setValue } from "../../actions/date.action";
import { setTodoDate, deleteTodoDate } from "../../actions/todo.action";
import { setTodosDate, deleteTodosDate } from "../../actions/todos.action";

import DeleteBtn from "../shared/DeleteBtn";

import "./index.scss";

const weekDay = ["Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"];

class DatePicker extends React.Component {
  state = {
    calendar: [],
  };

  componentDidMount() {
    this.setState({ calendar: this.builCalendar(this.props.value) });
  }

  componentDidUpdate(prevProps) {
    if (this.props.value !== prevProps.value) {
      this.setState({ calendar: this.builCalendar(this.props.value) });
    }
  }

  builCalendar(value) {
    const startDay = moment(value).clone().startOf("month").startOf("week");
    const endDay = moment(value).clone().endOf("month").endOf("week");
    const day = startDay.clone().subtract(1, "day");
    const tmpCalendar = [];

    while (day.isBefore(endDay, "day")) {
      tmpCalendar.push(
        Array(7)
          .fill(0)
          .map(() => day.add(1, "day").clone())
      );
    }
    return tmpCalendar;
  }

  render() {
    const { calendar } = this.state;
    const {
      setTodoDate,
      setTodosDate,
      toggleDate,
      setValue,
      deleteTodoDate,
      deleteTodosDate,
    } = this.props;
    const { id, value } = this.props;

    const setSelectedDate = (day) => {
      if (day.isBefore(new Date(), "day") === false) {
        setValue(day);
        setTodosDate(day, id);
        setTodoDate(day);
        toggleDate();
      }
    };

    const dayStyles = (day, value) => {
      //check if it's the same month
      if (
        moment(day).isBefore(value, "month") ||
        moment(day).isAfter(value, "month")
      )
        return "disabled";
      //check if the day is selected
      if (
        moment(value).isSame(day, "day") &&
        moment(value).isSame(day, "month") &&
        moment(value).isSame(day, "year")
      )
        return "selected";
      //check if the day is today
      if (moment(new Date()).isSame(day, "day")) return "today";
      return "";
    };

    const handleDelete = () => {
      deleteTodoDate();
      deleteTodosDate(id);
      setValue(moment());
      toggleDate();
    };

    return (
      <div className="picker-container">
        <div className="picker-header">
          <div
            className="picker-btn"
            onClick={() => {
              const day = moment(value).subtract(1, "year").format();
              setValue(day);
            }}
          >
            {String.fromCharCode(171)}
          </div>
          <div
            className="picker-btn simple"
            onClick={() => {
              const day = moment(value).subtract(1, "month").format();
              setValue(day);
            }}
          >
            {String.fromCharCode(60)}
          </div>
          <div className="header-content">
            <div className="picker-month">{moment(value).format("MMMM")}</div>
            <div className="picker-year">{moment(value).format("YYYY")}</div>
          </div>
          <div
            className="picker-btn simple"
            onClick={() => {
              const day = moment(value).add(1, "month").format();
              setValue(day);
            }}
          >
            {String.fromCharCode(62)}
          </div>
          <div
            className="picker-btn"
            onClick={() => {
              const day = moment(value).add(1, "year").format();
              setValue(day);
            }}
          >
            {String.fromCharCode(187)}
          </div>
        </div>
        <div className="days-content">
          {weekDay.map((day, index) => (
            <div key={index}>{day}</div>
          ))}
        </div>
        <div className="calendar">
          {calendar.map((week, wi) => (
            <div className="week" key={wi}>
              {week.map((day, di) => (
                <div
                  className="day"
                  key={di}
                  onClick={() => {
                    setSelectedDate(day);
                  }}
                >
                  <span className={dayStyles(day, value)}>
                    {day.format("D").toString()}
                  </span>
                </div>
              ))}
            </div>
          ))}
        </div>
        <DeleteBtn handleDelete={handleDelete} />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  setTodosDate: (day, id) => dispatch(setTodosDate(day, id)),
  setTodoDate: (day) => dispatch(setTodoDate(day)),
  deleteTodoDate: () => dispatch(deleteTodoDate()),
  deleteTodosDate: (id) => dispatch(deleteTodosDate(id)),
  toggleDate: () => dispatch(toggleDate()),
  setValue: (date) => dispatch(setValue(date)),
});

export default connect(null, mapDispatchToProps)(DatePicker);
