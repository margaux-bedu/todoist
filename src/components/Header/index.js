import React from "react";

import "./index.scss";

const Header = () => {
  return (
    <div className="header">
      <img src="/asset/logo.png" alt="Todoist" />
    </div>
  );
};

export default Header;
