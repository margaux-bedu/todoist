import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import { connect } from "react-redux";

import { setSelect } from "../../actions/todo.action";
import { setValue } from "../../actions/date.action";

import "./index.scss";

class TaskCard extends React.Component {
  render() {
    const { todo, selected, toggleTodo } = this.props;
    const { setSelect, setValue } = this.props;

    const displayDate = (date) => {
      if (
        moment(new Date()).isSame(date, "day") &&
        moment(new Date()).isSame(date, "month")
      )
        return "Aujourd'hui";
      else return moment(date).format("DD MMMM YYYY");
    };

    const handleClick = () => {
      if (selected === todo.id) {
        setSelect({}, null);
      } else {
        if (todo.date !== "") {
          setValue(todo.date);
        } else if (todo.date === "") {
          setValue(moment());
        }
        setSelect(todo, todo.id);
      }
    };

    return (
      <div
        className={
          selected === todo.id
            ? "task-container task-selected"
            : "task-container"
        }
        onClick={() => {
          handleClick();
        }}
      >
        <div className="task-wrapper">
          <div
            className={todo.completed ? "task-btn task-completed" : "task-btn"}
            onClick={() => {
              toggleTodo(todo.id);
            }}
          >
            <img src="/asset/white-check.png" alt="check" />
          </div>
          <div className={todo.completed ? "text-completed" : "none"}>
            <p className="task-title">{todo.title}</p>
            <p className="task-subtitle">{todo.user}</p>
          </div>
        </div>
        <p
          className={todo.completed ? "task-date text-completed" : "task-date"}
        >
          {todo.date === "" ? null : displayDate(todo.date)}
        </p>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  selected: state.todo.selectId,
});

const mapDispatchToProps = (dispatch) => ({
  setSelect: (todo, id) => dispatch(setSelect(todo, id)),
  setValue: (date) => dispatch(setValue(date)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TaskCard);

TaskCard.propTypes = {
  todo: PropTypes.object.isRequired,
  toggleTodo: PropTypes.func.isRequired,
};
