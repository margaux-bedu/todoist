import React from "react";
import { connect } from "react-redux";

import TaskCounter from "../TaskCounter";
import TaskCard from "../TaskCard";

import { addTodo, toggleTodo } from "../../actions/todos.action";
import { selectLastTodo } from "../../actions/todo.action";

import "./index.scss";

class SideList extends React.Component {
  state = {
    todoTitle: "",
  };

  render() {
    const { todoTitle } = this.state;
    const { todos } = this.props;
    const { addTodo, toggleTodo, selectLastTodo } = this.props;

    const completedTodos = todos.filter((todo) => todo.completed === true);

    const handleSubmit = (e) => {
      e.preventDefault();
      addTodo(todoTitle);
      selectLastTodo();
      this.setState({ todoTitle: "" });
    };

    return (
      <div className="side-container">
        <div className="side-title">
          <h3>Toutes les tâches</h3>
        </div>
        <form onSubmit={(e) => handleSubmit(e)}>
          <input
            value={todoTitle}
            type="text"
            name="todo"
            id="todo-input"
            placeholder="+ Ajouter une tâche..."
            onChange={(e) => this.setState({ todoTitle: e.target.value })}
          />
        </form>
        <div className="todos-list">
          <div>
            {todos
              .filter((todo) => {
                let filteredTodo = null;
                if (!todo.completed) {
                  filteredTodo = todo;
                }
                return filteredTodo;
              })
              .map((todo) => {
                return (
                  <TaskCard key={todo.id} todo={todo} toggleTodo={toggleTodo} />
                );
              })}
          </div>
          <TaskCounter count={completedTodos.length} />
          <div>
            {completedTodos.map((todo) => {
              return (
                <TaskCard key={todo.id} todo={todo} toggleTodo={toggleTodo} />
              );
            })}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  todos: state.todos,
});

const mapDispatchToProps = (dispatch) => ({
  addTodo: (title) => dispatch(addTodo(title)),
  toggleTodo: (id) => dispatch(toggleTodo(id)),
  selectLastTodo: () => dispatch(selectLastTodo()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SideList);
