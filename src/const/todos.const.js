export const ADD_TODO = "ADD_TODO";
export const TOGGLE_TODO = "TOGGLE_TODO";
export const SET_USER = "SET_USER";
export const SET_DATE = "SET_DATE";
export const SET_DESCRIPTION = "SET_DESCRIPTION";
export const DELETE_DATE = "DELETE_DATE";
export const DELETE_USER = "DELETE_USER";
