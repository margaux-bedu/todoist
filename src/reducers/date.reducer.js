import moment from "moment";
import * as dateConst from "../const/date.const";

const initState = {
  showDate: false,
  value: moment(),
};

const userReducer = (state = initState, action) => {
  switch (action.type) {
    case dateConst.TOGGLE_DATE: {
      return { ...state, showDate: !state.showDate };
    }
    case dateConst.SET_VALUE: {
      return { ...state, value: action.payload };
    }
    default:
      return state;
  }
};

export default userReducer;
