import * as userConst from "../const/user.const";

const initState = {
  userSelect: false,
  users: [
    {
      userName: "Alfredo Watts",
      userMail: "alfredo.watts@example.com",
      userAvatar: "https://randomuser.me/api/portraits/men/46.jpg",
    },
    {
      userName: "Joyce Mills",
      userMail: "joyce.mills@example.com",
      userAvatar: "https://randomuser.me/api/portraits/women/82.jpg",
    },
    {
      userName: "Adam Morrison",
      userMail: "adam.morrison@example.com",
      userAvatar: "https://randomuser.me/api/portraits/men/73.jpg",
    },
  ],
};

const userReducer = (state = initState, action) => {
  switch (action.type) {
    case userConst.TOGGLE_USER: {
      return { ...state, userSelect: !state.userSelect };
    }
    default:
      return state;
  }
};

export default userReducer;
