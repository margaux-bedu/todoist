import { combineReducers } from "redux";
import todos from "./todos.reducer";
import todo from "./todo.reducer";
import user from "./user.reducer";
import date from "./date.reducer";

export default combineReducers({
  todos,
  todo,
  user,
  date,
});
