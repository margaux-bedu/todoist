import * as todosConst from "../const/todos.const";

const initState = [];

const todosReducer = (state = initState, action) => {
  switch (action.type) {
    case todosConst.ADD_TODO: {
      const { id, title } = action.payload;
      return [
        ...state,
        {
          id: id,
          title: title,
          user: null,
          date: "",
          description: "",
          completed: false,
        },
      ];
    }
    case todosConst.TOGGLE_TODO: {
      return [
        ...state.map((todo) => {
          return todo.id === action.payload
            ? { ...todo, completed: !todo.completed }
            : todo;
        }),
      ];
    }
    case todosConst.SET_USER: {
      return [
        ...state.map((todo) => {
          const { id, name } = action.payload;
          return todo.id === id ? { ...todo, user: name } : todo;
        }),
      ];
    }
    case todosConst.SET_DATE: {
      return [
        ...state.map((todo) => {
          const { id, date } = action.payload;
          return todo.id === id ? { ...todo, date } : todo;
        }),
      ];
    }
    case todosConst.DELETE_DATE: {
      return [
        ...state.map((todo) => {
          return todo.id === action.payload ? { ...todo, date: "" } : todo;
        }),
      ];
    }
    case todosConst.DELETE_USER: {
      return [
        ...state.map((todo) => {
          return todo.id === action.payload ? { ...todo, user: null } : todo;
        }),
      ];
    }
    case todosConst.SET_DESCRIPTION: {
      return [
        ...state.map((todo) => {
          const { id, description } = action.payload;
          return todo.id === id ? { ...todo, description } : todo;
        }),
      ];
    }
    default:
      return state;
  }
};

export default todosReducer;
