import * as todoConst from "../const/todo.const";

const initState = {
  selectId: null,
  todo: {},
};

const todoReducer = (state = initState, action) => {
  switch (action.type) {
    case todoConst.SET_SELECT: {
      state = initState;
      const { id, todo } = action.payload;
      return { ...state, selectId: id, todo };
    }
    case todoConst.SET_TODO_DESCRIPTION: {
      return {
        ...state,
        todo: {
          ...state.todo,
          description: action.payload,
        },
      };
    }
    case todoConst.SET_TODO_USER: {
      return { ...state, todo: { ...state.todo, user: action.payload } };
    }
    case todoConst.SET_TODO_DATE: {
      return { ...state, todo: { ...state.todo, date: action.payload } };
    }
    case todoConst.DELETE_TODO_DATE: {
      return { ...state, todo: { ...state.todo, date: "" } };
    }
    case todoConst.DELETE_TODO_USER: {
      return { ...state, todo: { ...state.todo, user: null } };
    }
    default:
      return state;
  }
};

export default todoReducer;
